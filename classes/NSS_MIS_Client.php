<?php

/**
 * Class NSS_MIS_Client
 *
 * Class is responsible for sending the data to given endpoint, in order to simulate SOAP request.
 *
 */
class NSS_MIS_Client
{
    public static function sendCurlRequest($url, $data)
    {
        if (ENVIRONMENT === 'production') {
            //        echo 'debug: ' . PHP_EOL;
            //        echo 'sending data: ' . $data . PHP_EOL;
            //        echo 'to: ' . $url . PHP_EOL;
            $curl = self::prepareCurl($url, $data);
            $resp = curl_exec($curl);
            if (!$resp) {
                throw new \Exception(sprintf("Error while sending curl to %s: %s - %s",
                    $url, curl_error($curl), curl_errno($curl)));
            }
            preg_match('#<errorMessage>(.*?)</errorMessage>#', $resp, $error);
            if (isset($error[1]) && $error[1] == 'Connection failed.') {
                throw new \Exception('ERROR: Connection failed.');
            }
            //        preg_match('#<faultcode>(.*?)</faultcode>#', $resp, $error);
            if (strstr($resp, 'faultcode')) {
                throw new \Exception($resp);
            }
            preg_match('#<responseResult>(.*?)</responseResult>#', $resp, $result);
            if (isset($result[1]) && $result[1] == 'true') {
                return ['status' => true, 'data' => $resp];
            }

            return ['status' => false, 'data' => $resp];
        }

        return ['status' => false, 'data' => 'mocked request'];
    }

    public static function prepareCurl($url, $data)
    {
        $curl = curl_init();
        $headers = array(
            "Content-type: text/xml;charset=\"utf-8\"",
            "Accept: application/soap+xml",
            "Cache-Control: no-cache",
            "Pragma: no-cache",
            "Content-length: " . strlen($data),
        );

        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
            CURLOPT_USERAGENT => 'test',
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => $headers
        ));

        return $curl;
    }
}