<?php

class NSS_MIS_Item
{
//    private $url = 'http://80.74.163.206:8080/NonStopShopTest/services/RobaServisPort?wsdl';
    private $url = 'http://89.216.105.25:8081/NonStopServis/services/RobaServisPort?wsdl';

    private $data;

    private $ids = array();

    private $products = [];

    private $debug = 0;

    private $status = 0;

    public function __construct(WC_Product $product)
    {
        $this->compileRequestBodyItem($product, 1);
        $this->compileRequestBody();
        $this->sync = $this->sendRequest();

        update_field('synced', 1, $product->get_id());

//        echo sprintf('Synced %d items.', count($this->ids));
    }

    public function getSync()
    {
        return $this->sync;
    }

    public function getStatus() {
        return $this->status;
    }

    /**
     * Sends request to configured url.
     */
    public function sendRequest()
    {
        if ($this->debug) {
            var_dump('debug product sync:');
            echo $this->data;
            die();
        }
        try {
            $resp = NSS_MIS_Client::sendCurlRequest($this->url, $this->data);
            if (defined('WP_CLI') && WP_CLI) {
                echo 'item sync response: ' . PHP_EOL;
                var_dump($resp);
            }
            if ($resp['status'] !== true) {
                NSS_Log::log(print_r($resp['data'], true), NSS_Log::LEVEL_ERROR);
                var_dump($this->ids);
                var_dump($resp);
            }

            return true;
        } catch (\Exception $e) {
            echo 'error syncing item';
            var_dump($e->getMessage());
            NSS_Log::log($e->getMessage(), NSS_Log::LEVEL_ERROR);
        }

        return $this->ids[0];
    }

    /**
     * Compiles request body for SOAP impersonation request.
     *
     * @return void
     */
    public function compileRequestBody() {
        $this->data = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:q0="http://sifarnici.servis.mis.com/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <soapenv:Body>
    <q0:dodajRobu>'
            . $this->data .
            '</q0:dodajRobu>
  </soapenv:Body>
</soapenv:Envelope>';
    }

    /**
     * Compiles body list item for SOAP impersonation request.
     *
     * @param WC_Product  $item      item data
     * @param int    $increment increment
     */
    public function compileRequestBodyItem(WC_Product $item, $increment)
    {
        $pdv = 20;
        $priceNoPdv = round($item->get_price() * 100 / 120);
        if ($item->get_meta('pdv') == '10.00' || $item->get_meta('pdv') == '10') {
            $pdv = 10;
            $priceNoPdv = round($item->get_price() * 100 / 110);
        }
//        $name = str_replace(['Ø', '&', '/'], '', $item->get_name());
        $name = iconv("UTF-8", "ISO-8859-1//IGNORE", $item->get_name());
        $name = htmlspecialchars($name);

        $this->data .= "<robe>
            <aktivna>1</aktivna>
            <dodatniNazivRobe>". $item->get_sku() ."</dodatniNazivRobe>
            <kataloskiBroj>". $item->get_sku() ."</kataloskiBroj>
            <jedinicaMere>kom</jedinicaMere>
            <jeloIliPice>0</jeloIliPice>
            <klasifikacionaOznaka>01</klasifikacionaOznaka>
            <maloprodajnaCena>". $item->get_price() ."</maloprodajnaCena>
            <maloprodajnaCenaBezPpdv>". $priceNoPdv ."</maloprodajnaCenaBezPpdv>
            <nazivJediniceMere>komad</nazivJediniceMere>
            <nazivRobe>". $name ."</nazivRobe>
            <redniBrojSortiranja>". $increment ."</redniBrojSortiranja>
            <robaStandardnaKnjizenja>
              <sifraOrganizacije>01</sifraOrganizacije>
              <sifraStandardnogKnjizenja>01</sifraStandardnogKnjizenja>
            </robaStandardnaKnjizenja>
            <sifraTarifneGrupe>100</sifraTarifneGrupe>
            <skraceniNazivRobe>". $name ."</skraceniNazivRobe>
            <stopaPoreza>". $pdv ."</stopaPoreza>
            <vrstaKlasifikacije>5</vrstaKlasifikacije>
          </robe>";

        if (get_class($item) === WC_Product_Variable::class) {
            /* @var WC_Product_Variable $item */
            foreach ($item->get_variation_attributes() as $variations) {
                foreach ($variations as $variation) {
                    $increment++;
                    $this->data .= "<robe>
                <aktivna>1</aktivna>
                <dodatniNazivRobe>". $item->get_sku() . $variation ."</dodatniNazivRobe>
                <kataloskiBroj>". $item->get_sku() . $variation ."</kataloskiBroj>
                <jedinicaMere>kom</jedinicaMere>
                <jeloIliPice>0</jeloIliPice>
                <klasifikacionaOznaka>01</klasifikacionaOznaka>
                <maloprodajnaCena>". $item->get_price() ."</maloprodajnaCena>
                <maloprodajnaCenaBezPpdv>". $priceNoPdv ."</maloprodajnaCenaBezPpdv>
                <nazivJediniceMere>komad</nazivJediniceMere>
                <nazivRobe>". $name .' '. $variation ."</nazivRobe>
                <redniBrojSortiranja>". $increment ."</redniBrojSortiranja>
                <robaStandardnaKnjizenja>
                  <sifraOrganizacije>01</sifraOrganizacije>
                  <sifraStandardnogKnjizenja>01</sifraStandardnogKnjizenja>
                </robaStandardnaKnjizenja>
                <sifraTarifneGrupe>100</sifraTarifneGrupe>
                <skraceniNazivRobe>". $name .' '. $variation ."</skraceniNazivRobe>
                <stopaPoreza>". $pdv ."</stopaPoreza>
                <vrstaKlasifikacije>5</vrstaKlasifikacije>
                </robe>";
                }
            }
        }
    }
}