<?php

class NSS_MIS_Order
{
//    private $url = 'http://80.74.163.206:8080/NonStopShopTest/services/NarudzbenicaKupcaServisPort';
    private $url = 'http://89.216.105.25:8081/NonStopServis/services/NarudzbenicaKupcaServisPort?wsdl';

    private $data;

    /**
     * @var WC_Order
     */
    private $order;

    private $debug = 0;

    private $status = 0;

    public function __construct($order)
    {
        $this->order = $order;
        if (!$this->order->get_customer_id()) {
            $this->createCustomer();
        }
        NSS_Log::log('unosim order ' . $this->order->get_order_number(), NSS_Log::LEVEL_DEBUG);
//        echo 'unosim order ' . $this->order->get_order_number() . PHP_EOL;
        $this->compileRequestBodyItem();
        $this->compileRequestBody();
        $this->sendRequest();
    }

    private function createCustomer()
    {
        $email = md5($this->order) . '-telefonska@nonstopshop.rs';
        $user_id = wc_create_new_customer( $email, $email, ' % login - not - allowed % ' );
        update_user_meta( $user_id, "billing_first_name", $this->order->get_billing_first_name());
        update_user_meta( $user_id, "billing_last_name",  $this->order->get_billing_last_name());
        update_user_meta( $user_id, "billing_company",  $this->order->get_billing_company());
        update_user_meta( $user_id, "billing_email", $this->order->get_billing_email());
        update_user_meta( $user_id, "billing_phone",  $this->order->get_billing_phone());
        update_user_meta( $user_id, "billing_postcode",  $this->order->get_billing_postcode());
        update_user_meta( $user_id, "billing_city",  $this->order->get_billing_city());
        update_user_meta( $user_id, "billing_address_1",  $this->order->get_billing_address_1());
        update_user_meta( $user_id, "billing_address_2",  $this->order->get_billing_address_2());

        update_user_meta( $user_id, "shipping_first_name", $this->order->get_shipping_first_name() );
        update_user_meta( $user_id, "shipping_last_name",  $this->order->get_shipping_last_name());
        update_user_meta( $user_id, "shipping_company",  $this->order->get_shipping_company());
        update_user_meta( $user_id, "shipping_postcode",  $this->order->get_shipping_postcode());
        update_user_meta( $user_id, "shipping_city",  $this->order->get_shipping_city());
        update_user_meta( $user_id, "shipping_address_1",  $this->order->get_shipping_address_1());
        update_user_meta( $user_id, "shipping_address_2",  $this->order->get_shipping_address_2());

        $this->order->set_customer_id($user_id);
        $this->order->save();

        $user = get_user_by('id', $user_id);
        new NSS_MIS_User($user);
    }

    public function getStatus() {
        return $this->status;
    }

    /**
     * Sends request to configured url.
     */
    function sendRequest()
    {
        if ($this->debug) {
            var_dump('debug order sync:');
            echo $this->data;
            die();
        }
        try {
            $resp = NSS_MIS_Client::sendCurlRequest($this->url, $this->data);
            if ($resp['status'] !== true) {
                if (defined('WP_CLI')) {
                    var_dump($resp);
                }
                $regex = '#<errorMessage>(.*?)</errorMessage>#';
                preg_match($regex, $resp['data'], $matches);
                if (strpos($matches[1], 'Ne postoji partner za jmbg') !== false) {
                    $id = explode(':', $matches[1])[1];
                    NSS_Log::log('unosim partnera jer fali ' . $id, NSS_Log::LEVEL_ERROR);
                    //insert missing visitor
                    $this->status = 'entering missing visitor ' . $id;
                    if (!$id) {
                        var_dump($resp);
                        die('no id');
                    }
                    $user = get_userdata($id);
                    new NSS_MIS_User($user);
                } elseif (strpos($matches[1], 'Ne postoji roba za dodatni naziv') !== false) {
                    $sku = explode('Ne postoji roba za dodatni naziv', $matches[1])[1];
                    $sku = substr($sku, 0, 6);
                    $id = get_product_by_sku($sku);
                    if (!$id) {
                        return;
                    }
                    $product = wc_get_product($id);
                    //insert missing item
                    $msg = sprintf('entering item with sku: %s', $sku);
                    $this->status .= $msg;
//                    echo $this->status;

                    if (!$product) {
                        $error = sprintf("Could not find product with id : %s", $id);
                        NSS_Log::log($error, NSS_Log::LEVEL_ERROR);
                        return;
                    }
                    new NSS_MIS_Item($product);
                }

                // now sync order again
                $resp = NSS_MIS_Client::sendCurlRequest($this->url, $this->data);
                if ($resp['status'] === true) {
                    $this->order->add_meta_data('synced', 1);
                    $this->order->save_meta_data();
                    $this->status .= 'synced order ' . $this->order->get_order_number();
                }

//                }
            } else {
                $this->order->add_meta_data('synced', 1);
                $this->order->save_meta_data();
                $this->status = 'synced order ' . $this->order->get_order_number();
            }
        } catch (\Exception $e) {
            NSS_Log::log($e->getMessage(), NSS_Log::LEVEL_ERROR);
            echo $e->getMessage();
        }
        if (defined('WP_CLI') && WP_CLI) {
            echo 'status:  ' . $this->status;
        }
    }

    /**
     * Compiles request body for SOAP impersonation request.
     *
     * @return void
     */
    public function compileRequestBody() {
        $this->data = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:q0="http://dokumenti.servis.mis.com/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <soapenv:Body>
    <q0:dodajNarudzbenicuKupca>'
            . $this->data .
            '</q0:dodajNarudzbenicuKupca>
  </soapenv:Body>
</soapenv:Envelope>';
    }

    /**
     * Compiles body list item for SOAP impersonation request.
     *
     */
    public function compileRequestBodyItem()
    {
        $realizovano = 'false';
        if ($this->order->get_status() == 'finalizovano') {
            $realizovano = 'true';
        }
        $dt = new \DateTime($this->order->get_date_created());
        $time = str_replace('#', 'T', $dt->format('Y-m-d#H:i:s')) . 'Z';
        $this->data .= "<narudzbenice>
        <datumNaurdzbenice>". $time ."</datumNaurdzbenice>
        <deviznaNarudzbenica>false</deviznaNarudzbenica>
        <logname>online</logname>
        <organizacionaJedinica>011</organizacionaJedinica>
        <oznakaNarudzbenice>". $this->order->get_order_number() ."</oznakaNarudzbenice>
        <realizovano>". $realizovano ."</realizovano>
        <sifraPartnera>". $this->order->get_customer_id() ."</sifraPartnera>
        <sifraNacinaPlacanja>01</sifraNacinaPlacanja>" .
            $this->compileOrderItems() . PHP_EOL .
            $this->compileShipping() . PHP_EOL .
            "<storno>N</storno>
      </narudzbenice>";
    }

    public function compileShipping()
    {
        $data = '';
            $priceNoPdv = number_format($this->order->get_shipping_total() * 100 / 120, 2, '.', '');

            $data .= '<usluge>
          <cenaSaPorezom>'. $this->order->get_shipping_total() .'</cenaSaPorezom>
          <kolicina>1</kolicina>
          <osnovnaCenaBezporeza>'. $priceNoPdv .'</osnovnaCenaBezporeza>
          <osnovnaCenaSaRabatom>'. $priceNoPdv .'</osnovnaCenaSaRabatom>
          <sifraUsluge>001</sifraUsluge>
          <stopaPoreza>20</stopaPoreza>
          <stopaRabata>0</stopaRabata>
          <vrednostSaPorezom>'. $this->order->get_shipping_total() .'</vrednostSaPorezom>
        </usluge>';

        return $data;
    }

    public function compileOrderItems()
    {
        $data = '';
        /* @var WC_Order_Item_Product $item */
        foreach ($this->order->get_items() as $item) {
            $pdv = 20;
            $priceNoPdv = number_format($item->get_product()->get_price() * 100 / 120, 2, '.', '');
            $postMeta = get_post_meta($item->get_product_id(), 'pdv');
            $metaPdv = @$postMeta[0];
            if ($metaPdv == '10.00' || $metaPdv == '10' || !isset($postMeta[0])) {
                $pdv = 10;
                $priceNoPdv = number_format($item->get_product()->get_price() * 100 / 110, 2, '.', '');
            }
            $variation = '';
            if (get_class($item->get_product()) == WC_Product_Variation::class) {
                $variation = array_values($item->get_product()->get_attributes())[0];
            }

            $data .= '<stavke>
          <cenaSaPorezom>'. $item->get_product()->get_price() .'</cenaSaPorezom>
          <kolicina>'. $item->get_quantity() .'</kolicina>
          <osnovnaCenaBezporeza>'. $priceNoPdv .'</osnovnaCenaBezporeza>
          <osnovnaCenaSaRabatom>'. $priceNoPdv .'</osnovnaCenaSaRabatom>
          <osnovnavrednostSaSaRabatom>'. $priceNoPdv .'</osnovnavrednostSaSaRabatom>
          <sifraRobe>'. $item->get_product()->get_sku() . $variation .'</sifraRobe>
          <stopaPoreza>'. $pdv .'</stopaPoreza>
          <sifraMagacina>011</sifraMagacina>
          <stopaRabata>0</stopaRabata>
          <vrednostSaPorezom>'. $item->get_total() .'</vrednostSaPorezom>
        </stavke>';
        }

        return $data;
    }
}



