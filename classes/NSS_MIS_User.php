<?php

class NSS_MIS_User
{
//    private $url = 'http://80.74.163.206:8080/NonStopShopTest/services/PartnerServisPort?wsdl';
    private $url = 'http://89.216.105.25:8081/NonStopServis/services/PartnerServisPort?wsdl';

    private $data;

    private $resp;

    private $id;

    public function __construct(WP_User $user)
    {
        $this->id = $user->ID;
//        if (!$this->isSynced($user)) {
            foreach ([$user] as $row) {
                $this->compileRequestBodyItem($row);
            }
            $this->compileRequestBody();
            $this->sendRequest();
            update_user_meta($user->ID, 'synced', 1);
            $msg = sprintf('Synced user %s.', $user->ID);
            NSS_Log::log($msg, NSS_Log::LEVEL_DEBUG);
//            echo $msg;
//        } else {
//            echo sprintf('user %s already synced.', $user->ID);
//        }
    }

    private function isSynced(WP_User $user)
    {
        if (get_user_meta($user->ID, 'synced', true) != '1') {
            return false;
        }
        return true;
    }

    public function getResp()
    {
        return $this->resp;
    }

    /**
     * Sends request to configured url.
     */
    function sendRequest()
    {
//        echo $this->data;
//        die();

        $curl = curl_init();
        $headers = array(
            "Content-type: text/xml;charset=\"utf-8\"",
            "Accept: application/soap+xml",
            "Cache-Control: no-cache",
            "Pragma: no-cache",
            "Content-length: ".strlen($this->data),
        );

        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $this->url,
            CURLOPT_USERAGENT => 'test',
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $this->data,
            CURLOPT_HTTPHEADER => $headers
        ));
        $resp = curl_exec($curl);
        if(!$resp){
            die('ERROR: "' . curl_error($curl) . '" - Code: ' . curl_errno($curl));
        }
        curl_close($curl);
        $regex = '#<errorMessage>(.*?)</errorMessage>#';
//        var_dump($this->data);
        if (defined('WP_CLI') && WP_CLI) {
//            var_dump($resp);
//            var_dump($this->data);
        }
        preg_match($regex, $resp, $error);
        if (isset($error[1]) && $error[1] == 'Connection failed.') {
//            var_dump('ERROR: Connection failed.');
//            die();
        }

        $this->resp = $resp;
    }

    /**
     * Compiles request body for SOAP impersonation request.
     *
     * @return void
     */
    public function compileRequestBody() {
        $this->data = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:q0="http://sifarnici.servis.mis.com/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <soapenv:Body>
    <q0:dodajPartnera>'
            . $this->data .
            '</q0:dodajPartnera>
  </soapenv:Body>
</soapenv:Envelope>';
    }

    /**
     * Compiles body list item for SOAP impersonation request.
     *
     * @param WP_User $user
     */
    public function compileRequestBodyItem(WP_User $user)
    {
        $email = $user->user_email;

        if (get_user_meta($user->ID, 'shipping_first_name', true) == '') {
            $name = sprintf('%s %s', get_user_meta($user->ID, 'billing_first_name', true),
                get_user_meta($user->ID, 'billing_last_name', true));
        } else {
            $name = sprintf('%s %s', get_user_meta($user->ID, 'shipping_first_name', true),
                get_user_meta($user->ID, 'shipping_last_name', true));
        }

        $name = $this->transliterate($this->cleanUp($name));
//        $name = utf8_encode($name);

        $company = get_user_meta($user->ID, 'shipping_company', true);
        if ($company != '') {
            $company = get_user_meta($user->ID, 'billing_company', true);
        }
        if (strlen($company) > 3) {
            $name .= ' ' . $this->transliterate($company);
        }

        $zip = get_user_meta($user->ID, 'shipping_postcode', true);
        $country = get_user_meta($user->ID, 'shipping_country', true);
        $city = get_user_meta($user->ID, 'shipping_city', true);
        $phone = get_user_meta($user->ID, 'billing_phone', true);
        $pib = get_user_meta($user->ID, 'acf-field-pib', true);
        $fax = get_user_meta($user->ID, 'acf-field-fax', true);
        if ($zip == '') {
            $zip = get_user_meta($user->ID, 'billing_postcode', true);
        }
        if (get_user_meta($user->ID, 'shipping_address_1', true) == '') {
            $address = get_user_meta($user->ID, 'billing_address_1', true);
            $address .= ' ' . get_user_meta($user->ID, 'billing_address_2', true);
        } else {
            $address = get_user_meta($user->ID, 'shipping_address_1', true);
            $address .= ' ' . get_user_meta($user->ID, 'shipping_address_2', true);
        }
        $address = $this->transliterate($this->cleanUp($address));
        if ($country == '') {
            $country = get_user_meta($user->ID, 'billing_country', true);
        }
        if ($city == '') {
            $city = get_user_meta($user->ID, 'billing_city', true);
        }

        $pdv = 0;
        if ($pib != '') {
            $pdv = 1;
        }
        if ($phone == '') {
            $phone = '-';
        }
        if ($zip == '') {
            $zip = '11000';
        }

        $this->data .= "<partneri>
        <e_mail>". $email ."</e_mail>
        <fax>". $fax ."</fax>
        <jmbg>". $user->ID ."</jmbg>
        <matBroj>0</matBroj>
        <nazivDrzave>". $country ."</nazivDrzave>
        <nazivMesta>". $city ."</nazivMesta>
        <nazivPartnera>". $name ."</nazivPartnera>
        <oznakaDrzave>891</oznakaDrzave>
        <pib>". $pib ."</pib>
        <praSub>0</praSub>
        <pttBrojMesta>". $zip ."</pttBrojMesta>
        <sifraDelatnosti></sifraDelatnosti>
        <sifraDrzave>891</sifraDrzave>
        <sifraPodrucja>1</sifraPodrucja>
        <stampajBarkod>0</stampajBarkod>
        <statuspartnera>Aktivan</statuspartnera>
        <telefon>". $phone ."</telefon>
        <trziste>S02</trziste>
        <ulicaBroj>". $address ."</ulicaBroj>
        <uSistemuPdv>". $pdv ."</uSistemuPdv>
      </partneri>";
    }

    private function cleanUp($string)
    {
        $string = str_replace("&", "", $string);
        $string = str_replace("*", "", $string);
        $string = str_replace("/", "", $string);

        return $string;
    }

    private function transliterate($textcyr = null, $textlat = null) {
        $cyr = array(
            'ђ', 'ћ', 'ć', 'ж',  'ч',  'щ', 'њ', 'ш',  'ю',  'а', 'à', 'б', 'в', 'г', 'д', 'е', 'з', 'и', 'й', 'ј', 'к', 'л', 'љ', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ъ', 'ь', 'я',
            'Ђ', 'Ћ', 'Ć', 'Ж',  'Ч',  'Щ', 'Њ', 'Ш',  'Ю',  'А', 'А', 'Б', 'В', 'Г', 'Д', 'Е', 'З', 'И', 'Й', 'Ј', 'К', 'Л', 'Љ', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ъ', 'Ь', 'Я');
        $lat = array(
            'đ', 'c', 'c', 'ž', 'č', 'sht', 'nj', 'š', 'yu', 'a', 'a', 'b', 'v', 'g', 'd', 'e', 'z', 'i', 'j', 'j', 'k', 'l', 'lj', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'y', 'x', 'q',
            'Đ', 'C', 'C', 'Ž', 'Č', 'Sht', 'Nj', 'Š', 'Yu', 'A', 'А', 'B', 'V', 'G', 'D', 'E', 'Z', 'I', 'J', 'J', 'K', 'L', 'Lj', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F', 'H', 'c', 'Y', 'X', 'Q');
        if($textcyr) return str_replace($cyr, $lat, $textcyr);
        else if($textlat) return str_replace($lat, $cyr, $textlat);
        else return null;
    }
}
