<?php
namespace NSS\MIS;

class Pricelist
{
//    private $url = 'http://80.74.163.206:8080/NonStopShopTest/services/CenovnikServisPort';
    private $url = 'http://80.74.163.206:8080/NonStopServis/services/CenovnikServisPort';

    private $data;

    private $ids = array();

    private $debug = 0;

    private $status = 0;

    public function __construct($productId, $price)
    {
        $this->ids[] = $productId;
        $this->compileRequestBodyItem($price, $productId);
        $this->compileRequestBody();

        $this->status = $this->sendRequest();

//        echo sprintf('Synced new price %s for item %s', $price, $productId);
    }

    public function getStatus() {
        return $this->status;
    }

    /**
     * Sends request to configured url.
     */
    public function sendRequest()
    {
        if ($this->debug) {
            var_dump('debug product sync:');
            echo $this->data;
            die();
        }
        try {
            $resp = \NSS_MIS_Client::sendCurlRequest($this->url, $this->data);
            echo $this->data;
            var_dump($resp);
            if ($resp['status'] !== true) {
                echo $this->data;
                var_dump($resp['data']);
                \NSS_Log::log(print_r($resp['data'], true), \NSS_Log::LEVEL_ERROR);

                return $this->ids[0];
            }

            return true;
        } catch (\Exception $e) {
            \NSS_Log::log($e->getMessage(), \NSS_Log::LEVEL_ERROR);
        }
        return $this->ids[0];
    }

    /**
     * Compiles request body for SOAP impersonation request.
     *
     * @return void
     */
    public function compileRequestBody()
    {
        $this->data = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:q0="http://sifarnici.servis.mis.com/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <soapenv:Body>
    <q0:updateCenovnik>'.
            $this->data
            . '<oznakaCenovnika>01/170000000001</oznakaCenovnika>
    </q0:updateCenovnik>
  </soapenv:Body>
</soapenv:Envelope>';
    }

    public function compileRequestBodyItem($price, $itemId)
    {
        $this->data .= "<stavke>
        <logname>online</logname>
        <sifraJedMere>kom</sifraJedMere>
        <sifraRobe>". $itemId ."</sifraRobe>
        <tarifnaGrupa>100</tarifnaGrupa>
        <velCena>". $price ."</velCena>
      </stavke>" . PHP_EOL;
    }
}