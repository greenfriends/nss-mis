<?php
/**
 *   Plugin Name: Nss to M&I Systems order data transfer
 *   Plugin URI:
 *   Description: Order, user and item sync
 *   Author: Green friends
 *   Author URI:
 *   Version: 0.1
 */
// Prevent direct access
defined('ABSPATH') || die();
include("classes/NSS_MIS_Item.php");
include("classes/NSS_MIS_Order.php");
include("classes/NSS_MIS_User.php");
include("classes/Pricelist.php");
include("classes/NSS_MIS_Client.php");
include("classes/NSS_Log.php");

add_action('admin_menu', function (){
//    add_menu_page('Sync debug', 'Sync debug', 'edit_pages', 'nss-mis', 'misGetTabs');
});

function misGetTabs() {

//    NSS_Log::log('test', NSS_Log::LEVEL_ERROR);
//    die();

//    nssSyncOrder(300706);
//    $id = 300700;
//    $post = get_post($id);
//    var_dump((int) $product->get_meta('synced') !== 1);
//    die();
//    nssSyncUser(11);

    return '';
}

add_action('woocommerce_thankyou', 'nssSyncOrder', 666, 1);
function nssSyncOrder($orderId) {
    $order = wc_get_order($orderId);
    $misOrder = new NSS_MIS_Order($order);

//    NSS_Log::log($misOrder->getStatus());
}


add_action('woocommerce_process_product_meta', 'nssSyncItem', 666, 3);
//add_action('save_post_product', 'nssSyncItem', 666, 3);
//add_action( 'transition_post_status', 'nssSyncItem', 10, 3 );
function nssSyncItem($id, WP_Post $post) {
//function nssSyncItem($id, WP_Post $post, $update) {
//function nssSyncItem($newStatus, $oldStatus, WP_Post $post) {
    $product = wc_get_product($id);
    if ($product && strtolower($product->get_status()) != 'auto-draft' && strtolower($product->get_name()) != 'auto-draft'
    && $product->get_sku() != '') {
//    && $product->get_price() > 0) {
        try {
            if ((int) $product->get_meta('synced') !== 1) {
                new NSS_MIS_Item($product);
                $_POST['fields']['field_5b58cf2d4baa6'] = '1';
            }
        } catch (\Exception $e) {
            if ($e->getMessage() != 'no price') {
                NSS_Log::log($e->getMessage(), NSS_Log::LEVEL_ERROR);
            }
        }
    }
}

//add_action('user_register', 'nssSyncUser', 666, 1);
/**
 * Has to be manual, since MIS does not support update.
 * Use when user from order is missing.
 *
 * @param $userId
 */
function nssSyncUser($userId) {
    $user = get_userdata($userId);
    new NSS_MIS_User($user);
}

//add_action('update_product_meta', 'syncToMis', 10, 1);
//add_action('update_post_meta', 'syncToMis', 10, 4);
function syncToMis($meta_id, $object_id, $meta_key, $_meta_value) {
//    require_once(__DIR__ . "/../../../plugins/nss-mis/classes/Pricelist.php");
    $product = wc_get_product($object_id);
    if ($product && in_array($meta_key, ['_sale_price', '_price', '_regular_price'])) {
        switch ($meta_key) {
            case '_sale_price':

                if (get_class($product) === \WC_Product_Variable::class) {
                    foreach ($product->get_children() as $productId) {
                        $variation = wc_get_product($productId);
                        if ($variation->get_sale_price() != $_meta_value) {
                            new \NSS\MIS\Pricelist($product->get_sku(), $_meta_value);
                        }
                    }
                } else {
                    if ($product->get_sale_price() != $_meta_value) {
                        new \NSS\MIS\Pricelist($product->get_sku(), $_meta_value);
                    }
                }

                break;

            case '_price':

                if (get_class($product) === \WC_Product_Variable::class) {
                    foreach ($product->get_children() as $productId) {
                        $variation = wc_get_product($productId);
                        if ($variation->get_price() != $_meta_value) {
                            new \NSS\MIS\Pricelist($product->get_sku(), $_meta_value);
                        }
                    }
                } else {
                    if ($product->get_price() != $_meta_value) {
                        new \NSS\MIS\Pricelist($product->get_sku(), $_meta_value);
                    }
                }

                break;

//            case '_regular_price':
//                if ($product->get_regular_price() != $_meta_value) {
//                    new \NSS\MIS\Pricelist($object_id, $_meta_value);
//                }
//                break;

            default:

                break;
        }
    }
}